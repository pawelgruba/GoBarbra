﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;

namespace GoBarbra.SoundCore
{
	public class SoundCore
	{
		#region Constant fields
		private const string RESULT_FILE_NAME = "_final.wav";
		private const string SPEECH_TEMP_FILE_NAME = "_speech_temp.wav";
		private const string GO_BARBRA_MEDIA_FILES_LOCATION = "GoBarbraMediaFiles";

		#endregion

		#region Fields
		private SpeechSynthesizer _speech;
		private SoundPlayer _soundPlayer;
		private string _serverPath = string.Empty;
		private string resultFileName;
		private string speechTempFileName;

		private bool _isInWebMode;

		#endregion

		#region .ctor
		public SoundCore(bool isWebMode = false)
		{
			_isInWebMode = isWebMode;

			Initialization();
		}

		#endregion

		#region Properties
		public int SpeechSpeed
		{
			get
			{
				return _speech.Rate;
			}
			set
			{
				if (value < -10 || value > 10)
					throw new ArgumentOutOfRangeException("Voice Speed must be between -10 and 10");
				_speech.Rate = value;
			}
		}

		public int SpeechVolume
		{
			get
			{
				return _speech.Volume;
			}
			set
			{
				_speech.Volume = value;
			}
		}

		public string ServerPath
		{
			set
			{
				_serverPath = value;
				speechTempFileName = $"{value}\\{Guid.NewGuid().ToString()}_{SPEECH_TEMP_FILE_NAME}";
				resultFileName = $"{value}\\{Guid.NewGuid().ToString()}_{RESULT_FILE_NAME}";
			}
		}

		#endregion

		#region Methods
		public void PlayGoBarbra(string yourText)
		{
			try
			{
				GenerateAudioFileFromText(yourText);

				List<string> filesToCombine = new List<string>()
				{
					"empe3/ds_start.wav",
					speechTempFileName,
					"empe3/ds_end.wav"
				};

				Concatenate(resultFileName, filesToCombine);

				System.IO.File.Delete(speechTempFileName);

				_soundPlayer.Play();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public void GenerateGoBarbra(string path, string yourText)
		{
			try
			{
				GenerateAudioFileFromText(yourText);


			List<string> filesToCombine = new List<string>()
				{
					$"{_serverPath}\\{GO_BARBRA_MEDIA_FILES_LOCATION}\\ds_start.wav",
					speechTempFileName,
					$"{_serverPath}\\{GO_BARBRA_MEDIA_FILES_LOCATION}\\ds_end.wav"
				};

			Concatenate(path, filesToCombine);

			System.IO.File.Delete(speechTempFileName);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public void DeleteTemporaryFiles()
		{
			System.IO.File.Delete(speechTempFileName);
			System.IO.File.Delete(resultFileName);
		}

		private void GenerateAudioFileFromText(string yourText)
		{
			_speech.SetOutputToWaveFile(speechTempFileName+"_temp",
				new System.Speech.AudioFormat.SpeechAudioFormatInfo(44100, System.Speech.AudioFormat.AudioBitsPerSample.Sixteen,
				System.Speech.AudioFormat.AudioChannel.Stereo));
			_speech.Speak(yourText);
			_speech.Dispose();

			NAudio.Wave.WaveFileReader wfr = new WaveFileReader(speechTempFileName + "_temp");
			TrimWavFile(speechTempFileName + "_temp", speechTempFileName, new TimeSpan(0), wfr.TotalTime.Subtract(new TimeSpan(0, 0, 0, 0, 850)));
			
		}
		private void Concatenate(string outputFile, IEnumerable<string> sourceFiles)
		{
			byte[] buffer = new byte[1024];
			WaveFileWriter wfw = null;

			try
			{
				foreach (string sourceFile in sourceFiles)
				{
					using (WaveFileReader reader = new WaveFileReader(sourceFile))
					{
						if (wfw == null)
						{
							wfw = new WaveFileWriter(outputFile, reader.WaveFormat);
						}
						else
						{
							if (!reader.WaveFormat.Equals(wfw.WaveFormat))
							{
								throw new InvalidOperationException("Can't concatenate WAV Files that don't share the same format");
							}
						}

						int read;
						while ((read = reader.Read(buffer, 0, buffer.Length)) > 0)
						{
							wfw.Write(buffer, 0, read);
						}
					}
				}
			}
			finally
			{
				if (wfw != null)
				{
					wfw.Dispose();
				}
			}

		}

		private void Initialization()
		{
			if (_isInWebMode)
			{
				speechTempFileName = $"{Guid.NewGuid().ToString()}_{SPEECH_TEMP_FILE_NAME}";
				resultFileName = $"{Guid.NewGuid().ToString()}_{RESULT_FILE_NAME}";
			}
			else
			{
				resultFileName = RESULT_FILE_NAME;
				speechTempFileName = SPEECH_TEMP_FILE_NAME;
			}
			_soundPlayer = new SoundPlayer(resultFileName);
			_speech = new SpeechSynthesizer();
		}


			private  void TrimWavFile(string inPath, string outPath, TimeSpan cutFromStart, TimeSpan cutFromEnd)
			{
				using (WaveFileReader reader = new WaveFileReader(inPath))
				{
					using (WaveFileWriter writer = new WaveFileWriter(outPath, reader.WaveFormat))
					{
						int bytesPerMillisecond = reader.WaveFormat.AverageBytesPerSecond / 1000;

						int startPos = (int)cutFromStart.TotalMilliseconds * bytesPerMillisecond;
						startPos = startPos - startPos % reader.WaveFormat.BlockAlign;

						int endBytes = (int)cutFromEnd.TotalMilliseconds * bytesPerMillisecond;
						endBytes = endBytes - endBytes % reader.WaveFormat.BlockAlign;
						int endPos = (int)reader.Length - endBytes;

						TrimWavFile(reader, writer, startPos, endPos);
					}
				}
			}

			private static void TrimWavFile(WaveFileReader reader, WaveFileWriter writer, int startPos, int endPos)
			{
				reader.Position = startPos;
				byte[] buffer = new byte[1024];
				while (reader.Position < endPos)
				{
					int bytesRequired = (int)(endPos - reader.Position);
					if (bytesRequired > 0)
					{
						int bytesToRead = Math.Min(bytesRequired, buffer.Length);
						int bytesRead = reader.Read(buffer, 0, bytesToRead);
						if (bytesRead > 0)
						{
							writer.Write(buffer, 0, bytesRead);
						}
					}
				}
			}
		

		#endregion



	}
}
