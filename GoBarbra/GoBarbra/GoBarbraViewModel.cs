﻿using GalaSoft.MvvmLight.Command;
using System;
using System.ComponentModel;
using System.Windows.Input;

namespace GoBarbra
{
	public class GoBarbraViewModel : INotifyPropertyChanged
	{
		private string _yourText;
		
		private SoundCore.SoundCore _soundService = new SoundCore.SoundCore(false);

		public GoBarbraViewModel()
		{
			YourText = "Enter your text...";			
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged(String info)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(info));
			}
		}

		public string YourText
		{
			get { return this._yourText; }

			set
			{
				if (value != this._yourText)
				{
					this._yourText = value;
					NotifyPropertyChanged("_yourText");
				}
			}
		}

		private ICommand playGoBarbraCommand
		{
			get; set;
		}

		public ICommand PlayGoBarbraMix
		{
			get
			{
				if (playGoBarbraCommand == null)
					playGoBarbraCommand = new RelayCommand(PlayGoBarbraExecute, CanPlayGoBarbra);

				return playGoBarbraCommand;
			}
			internal set { }

		}

		private bool CanPlayGoBarbra()
		{
			return !string.IsNullOrEmpty(YourText);
		}

		private void PlayGoBarbraExecute()
		{
			try
			{
				_soundService.PlayGoBarbra(YourText);
			}
			catch (Exception)
			{

				
			}
		}

		public void DeleteFiles()
		{
			_soundService.DeleteTemporaryFiles();
		}



		
	}
}
