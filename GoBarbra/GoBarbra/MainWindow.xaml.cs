﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GoBarbra
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private GoBarbraViewModel _viewModel = new GoBarbraViewModel();
		public MainWindow()
		{
			InitializeComponent();
			this.DataContext = _viewModel;
		}

		private void TextBox_GotFocus(object sender, RoutedEventArgs e)
		{
			((TextBox)sender).Text = string.Empty;
			btnPlayStop.IsEnabled = false;
		}


		private void TextBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (_viewModel.YourText.Length > 0)
			{
				btnPlayStop.IsEnabled = true;
			}
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (((TextBox)sender).Text.Length > 0)

				btnPlayStop.IsEnabled = true;
			else
				btnPlayStop.IsEnabled = false;
			
		}

		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			_viewModel.DeleteFiles();
		}
	}
}
