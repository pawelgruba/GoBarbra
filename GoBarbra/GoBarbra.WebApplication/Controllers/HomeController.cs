﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace GoBarbra.WebApplication.Controllers
{
	public class HomeController : Controller
	{
		private GoBarbra.SoundCore.SoundCore _soundService = new SoundCore.SoundCore(true);
		public ActionResult GoBarbra()
		{
			return View();
		}

		public ActionResult PlayGoBarbra(string filePath)
		{
			ViewBag.Path = filePath;
			return View();
		}

		public async Task<ActionResult> GetGoBarbra(string Name)
		{
	
				_soundService.ServerPath = Server.MapPath("~/bin/");
				string fileName = System.IO.Path.Combine(Server.MapPath("~/Content/AppMedia"), Name + DateTime.Now.ToString("yyyyMMddHHmm") + ".wav");
				await Task.Run(() => _soundService.GenerateGoBarbra(fileName, Name));

				var resultPath = "/" + fileName.Substring(Server.MapPath("~/").Length).Replace("\\", "/");
				return new System.Web.Mvc.JsonResult() { JsonRequestBehavior = JsonRequestBehavior.AllowGet, Data = resultPath };


		}

		[HttpGet]
		public ActionResult GoBarbra2()
		{
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> GoBarbra2(string Name)
		{
			_soundService.ServerPath = Server.MapPath("~/bin/");
			string fileName = System.IO.Path.Combine(Server.MapPath("~/Content/AppMedia"), Name + DateTime.Now.ToString("yyyyMMddHHmm") + ".wav");
			await Task.Run(() => _soundService.GenerateGoBarbra(fileName, Name));

			var resultPath = "/" + fileName.Substring(Server.MapPath("~/").Length).Replace("\\", "/");
			return RedirectToAction("PlayGoBarbra", new { filePath = resultPath });
		}

		
	}
}